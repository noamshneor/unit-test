export function sum(...numbers: number[]) {
    return numbers.reduce((pre, cur) => pre + cur, 0);
}

export function multiply(multiplier: number, ...numbers: number[]) {
    return numbers.map(x => x * multiplier);
}