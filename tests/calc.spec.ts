import { expect } from "chai";
import { sum, multiply } from "../src/calc";

describe("The calc module", () => {
    context("#sum", () => {
        it("should exist", () => {
            expect(sum).to.be.a("function");
            expect(sum).to.be.instanceOf(Function);
        });
        it("should sum 2 numbers", () => {
            expect(sum(2, 3)).to.equal(5);
        });
        it("should sum several numbers", () => {
            expect(sum(2, 3, 5, 7, 7)).to.equal(24);
        });
    });
    context("#multiply", () => {
        it("should exist", () => {
            expect(multiply).to.be.a("function");
            expect(multiply).to.be.instanceOf(Function);
        });
        it("should mul 2 numbers", () => {
            expect(multiply(2, 3, 4)).to.eql([6, 8]);
        });
        it("should mul several numbers", () => {
            expect(multiply(3, 1, 2, 3, 4)).to.eql([3, 6, 9, 12]);
        });
    });
    context("#async tests", () => {
        const delay = (ms: number) => new Promise((res) => setTimeout(res, ms));
        it("should mul several numbers with delay", async () => {
            await delay(500);
            expect(multiply(3, 5, 2, 3, 4)).to.eql([15, 6, 9, 12]);
        });
    });
});